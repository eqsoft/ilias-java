FROM openjdk:8-jre-alpine
ARG BUILD_NO
RUN apk add --no-cache bash gettext curl
COPY init.sh loop.sh /usr/local/bin/
RUN chmod -R 755 /usr/local/bin
EXPOSE 11111
CMD ["/usr/local/bin/init.sh"]
